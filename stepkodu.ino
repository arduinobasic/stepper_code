#include <AccelStepper.h>
#include <AFMotor.h>

AF_Stepper motor(48,2);

void setup(){
  Serial.begin(9600);
  Serial.println("Stepper test");

  motor.setSpeed(500);//  rpm

  motor.step(25, FORWARD, MICROSTEP);
  motor.release();
  delay(1500);
  motor.release();
}

void loop(){
motor.step(500, FORWARD, MICROSTEP);
motor.release();
delay(1000);
motor.step(500, BACKWARD, MICROSTEP);
motor.release();
delay(1000);
}


